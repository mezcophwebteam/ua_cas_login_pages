# UA CAS Login Pages #

As part of best practices at the University, we log in to our Drupal sites using the University's Central Authentication System (CAS).
Additionally, to help prevent form spam, we rewrite the `/user` and `/user/login` pages so that they just provide a link to our CAS login.

This small module does exactly that, but it can be disabled.
This is especially useful when pulling down copies of a site to a test or development server because you're one drush command away from being able to log in to the site.
